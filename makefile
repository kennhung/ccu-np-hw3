SHELL = /bin/bash
CC = gcc
CFLAGS = -g -lpcap
SRC = main.c
EXE = $(patsubst %.c, %, $(SRC))

all: ${EXE}

%:	%.c
	${CC} ${CFLAGS} $@.c -o $@

clean:
	rm ${EXE}

