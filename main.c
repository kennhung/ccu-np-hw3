#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <pcap.h>
#include <arpa/inet.h>
#include <netinet/ether.h>
#include <netinet/ip.h>
#include <netinet/ip6.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>

// #define DEBUG

char* get_ether_type_string(uint16_t type);

void handle_ip_packet(struct ip* iphdr);
void handle_ip6_packet(struct ip6_hdr* ip6hdr);

void handle_tcp_packet(struct tcphdr* tcphdr);
void handle_udp_packet(struct udphdr* tcphdr);

char buf[8][4096];

int main(int argc, char** argv) {
    if (argc < 2) {
        fprintf(stderr, "Missing pcap file\n");
        exit(1);
    }

    char errbuf[1024] = {0};
    pcap_t* handle = pcap_open_offline(argv[1], errbuf);
    if (handle == NULL) {
        fprintf(stderr, "Error: %s\n", errbuf);
        exit(1);
    }

    struct pcap_pkthdr* hdr;
    const u_char* packet;

    int retval;
    while ((retval = pcap_next_ex(handle, &hdr, &packet)) == 1) {
        for (int i = 0; i < 8; i++) buf[i][0] = 0;

        const struct ether_header* ethernetHeader =
            (struct ether_header*)packet;

#ifdef DEBUG
        printf("Time: %ld.%06ld\n", hdr->ts.tv_sec, hdr->ts.tv_usec);
        printf("Source MAC: %s\n",
               ether_ntoa((struct ether_addr*)ethernetHeader->ether_shost));
        printf("Dest MAC: %s\n",
               ether_ntoa((struct ether_addr*)ethernetHeader->ether_dhost));

        printf("Ethernet type: %s\n",
               get_ether_type_string(ntohs(ethernetHeader->ether_type)));
#endif

        // time
        sprintf(buf[0], "%ld.%06ld", hdr->ts.tv_sec, hdr->ts.tv_usec);
        // s_mac
        strcpy(buf[1],
               ether_ntoa((struct ether_addr*)ethernetHeader->ether_shost));
        // d_mac
        strcpy(buf[2],
               ether_ntoa((struct ether_addr*)ethernetHeader->ether_dhost));
        // eth_type
        strcpy(buf[3],
               get_ether_type_string(ntohs(ethernetHeader->ether_type)));

        if (ntohs(ethernetHeader->ether_type) == ETHERTYPE_IP) {
            struct ip* iphdr =
                (struct ip*)(packet + sizeof(struct ether_header));

            handle_ip_packet(iphdr);

            if (iphdr->ip_p == IPPROTO_TCP) {
                struct tcphdr* tcphdr =
                    (struct tcphdr*)(packet + sizeof(struct ether_header) +
                                     sizeof(struct ip));

                handle_tcp_packet(tcphdr);
            } else if (iphdr->ip_p == IPPROTO_UDP) {
                struct udphdr* udphdr =
                    (struct udphdr*)(packet + sizeof(struct ether_header) +
                                     sizeof(struct ip));

                handle_udp_packet(udphdr);
            }
        } else if (ntohs(ethernetHeader->ether_type) == ETHERTYPE_IPV6) {
            struct ip6_hdr* ip6hdr =
                (struct ip6_hdr*)(packet + sizeof(struct ether_header));

            handle_ip6_packet(ip6hdr);

            if (ip6hdr->ip6_nxt == IPPROTO_TCP) {
                struct tcphdr* tcphdr =
                    (struct tcphdr*)(packet + sizeof(struct ether_header) +
                                     sizeof(struct ip6_hdr));

                handle_tcp_packet(tcphdr);
            } else if (ip6hdr->ip6_nxt == IPPROTO_UDP) {
                struct udphdr* udphdr =
                    (struct udphdr*)(packet + sizeof(struct ether_header) +
                                     sizeof(struct ip6_hdr));

                handle_udp_packet(udphdr);
            }
        }

#ifdef DEBUG
        printf("\n");
#endif

#ifndef DEBUG
        for (int i = 0; i < 8; i++) {
            if (buf[i][0] == 0) break;
            if (i != 0) printf(" ");
            printf("%s", buf[i]);
        }
        printf("\n");

#endif
    }

    // printf("retval = %d\n", retval);

    if (retval == PCAP_ERROR) {
        pcap_perror(handle, "error: ");
        exit(1);
    }

    return 0;
}

void handle_ip_packet(struct ip* iphdr) {
#ifdef DEBUG
    printf("Source IP: %s\n", inet_ntoa(iphdr->ip_src));
    printf("Dest IP: %s\n", inet_ntoa(iphdr->ip_dst));
#endif
    // s_ip
    strcpy(buf[4], inet_ntoa(iphdr->ip_src));
    // d_ip
    strcpy(buf[5], inet_ntoa(iphdr->ip_dst));
}

void handle_ip6_packet(struct ip6_hdr* ip6hdr) {
    char srcip[INET6_ADDRSTRLEN];
    char dstip[INET6_ADDRSTRLEN];

    if (inet_ntop(AF_INET6, &(ip6hdr->ip6_src), srcip, sizeof(srcip)) == NULL) {
        perror("inet_ntop");
        exit(EXIT_FAILURE);
    }

    if (inet_ntop(AF_INET6, &(ip6hdr->ip6_dst), dstip, sizeof(dstip)) == NULL) {
        perror("inet_ntop");
        exit(EXIT_FAILURE);
    }

#ifdef DEBUG
    printf("Source IP: %s\n", srcip);
    printf("Dest IP: %s\n", dstip);
#endif
    // s_ip
    strcpy(buf[4], srcip);
    // d_ip
    strcpy(buf[5], dstip);
}

void handle_tcp_packet(struct tcphdr* tcphdr) {
#ifdef DEBUG
    printf("Source Port: %d\n", htons(tcphdr->th_sport));
    printf("Dest Port: %d\n", htons(tcphdr->th_dport));
#endif

    // s_port
    sprintf(buf[6], "%d", htons(tcphdr->th_sport));
    // d_port
    sprintf(buf[7], "%d", htons(tcphdr->th_dport));
}

void handle_udp_packet(struct udphdr* udphdr) {
#ifdef DEBUG
    printf("Source Port: %d\n", htons(udphdr->uh_sport));
    printf("Dest Port: %d\n", htons(udphdr->uh_dport));
#endif

    // s_port
    sprintf(buf[6], "%d", htons(udphdr->uh_sport));
    // d_port
    sprintf(buf[7], "%d", htons(udphdr->uh_dport));
}

char* get_ether_type_string(uint16_t type) {
    switch (type) {
        case ETHERTYPE_PUP:
            return "Xerox PUP";
        case ETHERTYPE_SPRITE:
            return "Sprite";
        case ETHERTYPE_IP:
            return "IP";
        case ETHERTYPE_ARP:
            return "ARP";
        case ETHERTYPE_REVARP:
            return "REVARP";
        case ETHERTYPE_AT:
            return "AppleTalk";
        case ETHERTYPE_AARP:
            return "AppleTalk ARP";
        case ETHERTYPE_VLAN:
            return "VLAN";
        case ETHERTYPE_IPX:
            return "IPX";
        case ETHERTYPE_IPV6:
            return "IPV6";
        case ETHERTYPE_LOOPBACK:
            return "LOOPBACK";
        default:
            return "Unknown";
    }
}
